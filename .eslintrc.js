module.exports = {
  rules: {
    "no-console": "on"
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      modules: true
    }
  }
};
