# Tino-IM

## Features:
* Registration page with avatar selection
* Login and basic authentication
* Add a topic via topic ID
* Basic message history and message sending/receiving
